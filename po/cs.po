# Czech translation for sturmreader.emanuelesorce
# Copyright (C) 2021 Emanuele Sorce, Robert Schroll and others
# This file is distributed under the same license as the sturmreader.emanuelesorce package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: sturmreader.emanuelesorce\n"
"Report-Msgid-Bugs-To: emanuele.sorce@hotmail.com\n"
"POT-Creation-Date: 2021-09-02 22:46+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ui/qml/ContentDialog.qml:32
msgid "Contents"
msgstr "Obsah"

#: ui/qml/ContentDialog.qml:45
msgid "Outline"
msgstr "Přehled"

#: ui/qml/ContentDialog.qml:52
msgid "Pages"
msgstr "Strany"

#: ui/qml/ContentDialog.qml:60
msgid "Bookmarks"
msgstr "Záložky"

#: ui/qml/ContentDialog.qml:101 ui/qml/ContentDialog.qml:155
#: ui/qml/ContentDialog.qml:311
msgid "Page"
msgstr "Strana"

#: ui/qml/ContentDialog.qml:127
msgid "No outline available"
msgstr "Přehled není k dispozici"

#: ui/qml/ContentDialog.qml:226
msgid "purple"
msgstr "fialová"

#: ui/qml/ContentDialog.qml:226
msgid "blue"
msgstr "modrá"

#: ui/qml/ContentDialog.qml:226
msgid "red"
msgstr "červená"

#: ui/qml/ContentDialog.qml:226
msgid "green"
msgstr "zelená"

#: ui/qml/ContentDialog.qml:226
msgid "yellow"
msgstr "žlutá"

#: ui/qml/ContentDialog.qml:259
msgid "<font color='red'>Delete</font>"
msgstr "<font color='red'>Odstranit</font>"

#: ui/qml/ContentDialog.qml:271
msgid "<b>Set at current page</b>"
msgstr "<b>Nastavit</b>"

#: ui/qml/ContentDialog.qml:306
msgid "Bookmark"
msgstr "Záložka"

#: ui/qml/ContentDialog.qml:317
msgid "(Empty)"
msgstr "(prázdná)"

#: ui/qml/Settings.qml:28
msgid "Settings"
msgstr "Nastavení"

#: ui/qml/Settings.qml:59
msgid "Default Book Location"
msgstr "Výchozí umístění knih"

#: ui/qml/Settings.qml:63
msgid ""
"Sturm Reader seems to be operating under AppArmor restrictions that prevent "
"it from accessing most of your home directory.  Ebooks should be put in <i>"
"%1</i> for Sturm Reader to read them."
msgstr ""
"Čtečka Sturm pracuje v rámci omezení AppArmor, které brání v přístupu k "
"větší části tvého domovského adresáře.  Elektronické knihy by proto měly "
"být vloženy do adresáře <i>%1</i>, aby je čtečka Sturm mohla nalézt."

#: ui/qml/Settings.qml:71
msgid "Reload Directory"
msgstr "Znovu hledat"

#: ui/qml/Settings.qml:156
msgid "Keep display on while reading"
msgstr "Nevypínat obrazovku při čtení"

#: ui/qml/Settings.qml:179
msgid "Application Style (experimental)"
msgstr "Motivy prostředí (ve vývoji)"

#: ui/qml/Settings.qml:184
msgid "Supported styles: "
msgstr "Podporované motivy: "

#: ui/qml/Settings.qml:190
msgid "Requires a restart to take effect"
msgstr "Vyžaduje opětovné spuštění"

#: ui/qml/ImporterUT.qml:51
msgid "Import books from"
msgstr "Přidat knihy z"

#: ui/qml/Reader.qml:22
msgid "Could not determine file type."
msgstr "Neznámý formát souboru."

#: ui/qml/Reader.qml:22
msgid ""
"Remember, Sturm Reader can only open EPUB, PDF, and CBZ files without DRM."
msgstr ""
"Čtečka Sturm může otevřít pouze soubory ve formátu EPUB, PDF a CBZ bez DRM."

#: ui/qml/Reader.qml:23
msgid "Could not parse file."
msgstr "Soubor nelze zpracovat."

#: ui/qml/Reader.qml:23
msgid ""
"Although it appears to be a %1 file, it could not be parsed by Sturm Reader."
msgstr ""
"Přestože se zdá, že jde o %1 soubor, čtečka Sturm jej neumí zpracovat."

#: ui/qml/ImporterPortable.qml:23
msgid "Choose the books to import"
msgstr "Vyber knihy"

#: ui/qml/LocalBooks.qml:41
msgid "Library"
msgstr "Knihovna"

#: ui/qml/LocalBooks.qml:69
msgid "Recently Read"
msgstr "Dle čtenosti"

#: ui/qml/LocalBooks.qml:72
msgid "Title"
msgstr "Dle názvu"

#: ui/qml/LocalBooks.qml:75
msgid "Author"
msgstr "Dle autora"

#: ui/qml/LocalBooks.qml:96
msgid "Loading library..."
msgstr "Nahrávám knihovnu..."

#: ui/qml/LocalBooks.qml:96
msgid "Processing books data..."
msgstr "Zpracovávám knihy..."

#: ui/qml/LocalBooks.qml:116
msgid "Welcome to Sturm Reader!"
msgstr "Vítejte ve čtečce Sturm!"

#: ui/qml/LocalBooks.qml:228
msgid "Could not open this book."
msgstr "Tuto knihu nelze otevřít."

#: ui/qml/LocalBooks.qml:505
msgid "Back"
msgstr "Zpět"

#: ui/qml/LocalBooks.qml:613
msgid "Unknown Author"
msgstr "Neznámý autor"

#: ui/qml/LocalBooks.qml:614
msgid "%1 Book"
msgid_plural "%1 Books"
msgstr[0] "%1 kniha"
msgstr[1] "%1 knihy"
msgstr[2] "%1 knih"

#: ui/qml/LocalBooks.qml:702
msgid "No Books in Library"
msgstr "V knihovně nejsou žádné knihy"

#. / A path on the file system. /
#: ui/qml/LocalBooks.qml:711
msgid ""
"Sturm Reader could not find any books for your library, and will "
"automatically find all epub files in <i>%1</i>.  Additionally, any book "
"opened will be added to the library."
msgstr ""
"Čtečka Sturm nenalezla žádnou knihu, aby ji přidala do tvé knihovny. Bude "
"automaticky hledat všechny soubory ve formátu EPUB v adresáři <i>%1</i>.  "
"Do knihovny bude přidána i každá nově otevřená kniha."

#: ui/qml/LocalBooks.qml:721
msgid "Get Books"
msgstr "Přidat knihy"

#: ui/qml/LocalBooks.qml:729
msgid "Search Again"
msgstr "Znovu hledat"

#. / A control can be dragged to delete a file.  The deletion occurs /
#. / when the user releases the control. /
#: ui/qml/LocalBooks.qml:807
msgid "Release to Delete"
msgstr "Odstranit uvolněním"

#. / A control can be dragged to delete a file. /
#: ui/qml/LocalBooks.qml:809
msgid "Swipe to Delete"
msgstr "Odstranit tahem"

#: ui/qml/Converter.qml:37
msgid "Conversion required"
msgstr "Je nutný převod"

#: ui/qml/Converter.qml:77
msgid ""
"The file that you tried to open needs to be converted to a format supported "
"by Sturm Reader to be opened."
msgstr ""
"Soubor, který se pokoušíš otevřít, je třeba převést do formátu pro čtečku "
"Sturm."

#: ui/qml/Converter.qml:78
msgid ""
"This process is automatic and the book will be then available in your "
"library."
msgstr ""
"Tento proces je automatický a kniha pak bude k dispozici ve tvé knihovně."

#: ui/qml/Converter.qml:79
msgid "Press the button to start the process"
msgstr "Stistkni tlačítko pro zahájení procesu"

#: ui/qml/Converter.qml:94
msgid "Convert"
msgstr "Převést"

#: ui/qml/Converter.qml:111
msgid ""
"Conversion failed: you can try importing the book again. You may check the "
"logs for more details, or open an issue for support"
msgstr ""
"Převod selhal: zkus knihu znovu přidat. Pro více informací můžeš zkontrolovat "
"logy případně vytvořit chybové hlášení"

#: ui/qml/About.qml:25
msgid "About"
msgstr "O aplikaci"

#: ui/qml/About.qml:70
msgid "Sturm Reader"
msgstr "Čtečka Sturm"

#: ui/qml/About.qml:78
msgid ""
"Sturm (und Drang) Reader is an open source Ebook reader.<br>Community is "
"what makes this app possible, so pull requests, translations, feedback and "
"donations are very appreciated :)<br>This app Is a fork of the Beru app by "
"Rshcroll, Thanks!<br>This app stands on the shoulder of various Open Source "
"projects, see source code for licensing details"
msgstr ""
"Čtečka Sturm (und Drang) je open source čtečka elektronických knih.<br>Tuto "
"aplikaci tvoří komunita, proto jsou velmi vítány návrhy, překlady, zpětná "
"vazba a finanční příspěvky. :)<br>Čtečka vychází z aplikace Beru od Roberta "
"Schrolla, děkujeme!<br>Základ čtečky tvoří různé open source projekty, viz "
"licenční podrobnosti zdrojového kódu."

#: ui/qml/About.qml:86
msgid ""
"A big thanks to all translators, beta-testers, and users<br/>in general who "
"improve this app with their work and feedback"
msgstr ""
"Velké poděkování patří všem překladatelům, beta testerům a uživatelům,<br/>kteří "
"svou prací a zpětnou vazbou tuto čtečku pomáhají vylepšit."

#: ui/qml/About.qml:94
msgid "A special thanks to Joan Cibersheep for the new logo"
msgstr "Zvláštní poděkování za nové logo patří Joanu Cibersheepovi."

#: ui/qml/About.qml:102
msgid "A special thanks to Jeroen for support and a test device"
msgstr "Zvláštní poděkování za podporu a testovací zařízení patří Jeroenovi."

#: ui/qml/About.qml:107
msgid "❤Donate❤"
msgstr "❤ Přispět finančně ❤"

#: ui/qml/About.qml:115
msgid "See source on:"
msgstr "Zobrazit zdrojový kód na serveru"

#: ui/qml/About.qml:123
msgid "Report bug or feature request"
msgstr "Oznámit chybu nebo vylepšení"

#: ui/qml/About.qml:131
msgid "See License (GNU GPL v3)"
msgstr "Zobrazit licenci (GNU GPL v3)"

#: ui/qml/ImportPage.qml:42
msgid "Importing books..."
msgstr "Přidávám knihy..."

#: ui/qml/ImportPage.qml:42
msgid "Books imported"
msgstr "Knihy byly přidány"

#: ui/qml/ImportPage.qml:76
msgid "Waiting"
msgstr "Čekám"

#: ui/qml/ImportPage.qml:78
msgid "Processing"
msgstr "Zpracovávám"

#: ui/qml/ImportPage.qml:80
msgid "Imported to %1"
msgstr "Přidána do knihovny: %1"

#: ui/qml/ImportPage.qml:82
msgid "Already in library: %1"
msgstr "V knihovně se již nachází: %1"

#: ui/qml/ImportPage.qml:84
msgid "Error: %1"
msgstr "Chyba: %1"

#: ui/qml/Main.qml:119
msgid "Error Opening File"
msgstr "Chyba při otevírání souboru"

#: ui/qml/MetricsUT.qml:13
msgid "Pages read today: %1"
msgstr "Dnes přečtených stránek: %1"

#: ui/qml/MetricsUT.qml:14
msgid "No pages read today"
msgstr "Dnes nic nepřečteno"

#: ui/qml/BookSettingsDialog.qml:49
msgid "Book Settings"
msgstr "Nastavení knihy"

#. / Prefer string of < 16 characters /
#: ui/qml/BookSettingsDialog.qml:96
msgid "Background color"
msgstr "Barva pozadí"

#: ui/qml/BookSettingsDialog.qml:110
msgid "White"
msgstr "Bílá"

#: ui/qml/BookSettingsDialog.qml:117
msgid "Light"
msgstr "Světlá"

#: ui/qml/BookSettingsDialog.qml:124
msgid "Dark"
msgstr "Tmavá"

#: ui/qml/BookSettingsDialog.qml:130
msgid "Black"
msgstr "Černá"

#. / Prefer string of < 16 characters /
#: ui/qml/BookSettingsDialog.qml:160
msgid "Color scheme"
msgstr "Barva písma"

#: ui/qml/BookSettingsDialog.qml:174
msgid "Black on White"
msgstr "Černé na bílém"

#: ui/qml/BookSettingsDialog.qml:182
msgid "Dark on Texture"
msgstr "Tmavé na textuře"

#: ui/qml/BookSettingsDialog.qml:190
msgid "Light on Texture"
msgstr "Světlé na textuře"

#: ui/qml/BookSettingsDialog.qml:197
msgid "White on Black"
msgstr "Bílé na černém"

#. / Prefer string of < 16 characters /
#: ui/qml/BookSettingsDialog.qml:228
msgid "Quality"
msgstr "Kvalita"

#. / Prefer string of < 16 characters /
#: ui/qml/BookSettingsDialog.qml:252
msgid "Font"
msgstr "Font písma"

#: ui/qml/BookSettingsDialog.qml:262 ui/qml/BookSettingsDialog.qml:272
msgid "Default Font"
msgstr "Výchozí font"

#. / Prefer string of < 16 characters /
#: ui/qml/BookSettingsDialog.qml:285
msgid "Font Scaling"
msgstr "Velikost písma"

#. / Prefer string of < 16 characters /
#: ui/qml/BookSettingsDialog.qml:308
msgid "Line Height"
msgstr "Výška řádku"

#. / Prefer string of < 16 characters /
#: ui/qml/BookSettingsDialog.qml:331
msgid "Margins"
msgstr "Velikost okraje"

#. / Prefer < 16 characters /
#: ui/qml/BookSettingsDialog.qml:352
msgid "Make Default"
msgstr "Uložit jako výchozí nastavení"

#. / Prefer < 16 characters /
#: ui/qml/BookSettingsDialog.qml:360
msgid "Load Defaults"
msgstr "Načíst výchozí nastavení"
