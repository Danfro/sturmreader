/* Copyright 2015 Robert Schroll
 * Copyright 2021 Emanuele Sorce emanuele.sorce@hotmail.com
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

#include "comicreader.h"
#include "../mimetype.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QtGui/QImage>
#include <QBuffer>
#include <QDir>
#include <QString>
#include <QCryptographicHash>
#include <QDebug>
#include <quazip.h>
#include <quazipfile.h>
#include "../qhttpserver/qhttpresponse.h"
#include "../qtrar/src/qtrarfile.h"
#include "../qtrar/src/qtrarfileinfo.h"

const int TYPE_CBZ = 0;
const int TYPE_CBR = 1;

ComicReader::ComicReader(QObject *parent) : QObject(parent)
{
	zip = NULL;
	rar = NULL;
}

bool ComicReader::load(const QString &filename)
{
	_title = filename.split('/').last().chopped(4);

	type = guessMimeType(filename) == "application/zip" ? TYPE_CBZ : TYPE_CBR;

	if (zip != NULL)
	{
		delete zip;
		zip = NULL;
	}
	if (rar != NULL)
	{
		delete rar;
		rar = NULL;
	}
	_hash = "";
	spine.clear();

	if (type == TYPE_CBZ)
	{
		zip = new QuaZip(filename);
		if (!zip->open(QuaZip::mdUnzip))
		{
			delete zip;
			zip = NULL;
			return false;
		}
	}
	else
	{
		rar = new QtRAR(filename);
		if (!rar->open(QtRAR::OpenModeExtract))
		{
			delete rar;
			rar = NULL;
			return false;
		}
	}

	if (!parse())
	{
		if (type == TYPE_CBZ)
		{
			delete zip;
			zip = NULL;
		}
		else
		{
			delete rar;
			rar = NULL;
		}
		return false;
	}

	return true;
}

int ComicReader::numberOfPages()
{
	if (type == TYPE_CBZ && !zip)
	{
		qDebug() << "Comic file not open";
		return 0;
	}
	if (type == TYPE_CBR && !rar)
	{
		qDebug() << "Comic file not open";
		return 0;
	}
	return spine.length();
}

bool ComicReader::parse()
{
	QList<QuaZipFileInfo> fileListZ;
	QList<QtRARFileInfo> fileListR;

	if (type == TYPE_CBZ)
		fileListZ = zip->getFileInfoList();

	if (type == TYPE_CBR)
		fileListR = rar->fileInfoList();

	// config files and metadata - ignored but don't fail
	// See https://wiki.mobileread.com/wiki/CBR_and_CBZ
	QStringList ignored_extensions = {
		".xml", ".config", ".json", ".acbf"};

	if (type == TYPE_CBZ)
	{
		foreach (const QuaZipFileInfo info, fileListZ)
		{
			if (info.uncompressedSize > 0)
			{
				bool valid = true;
				for (int e = 0; e < ignored_extensions.length(); e++)
				{
					if (info.name.endsWith(ignored_extensions[e], Qt::CaseInsensitive))
					{
						valid = false;
						break;
					}
				}
				if (valid)
					spine.append(info.name);
			}
		}
	}
	else
	{
		foreach (const QtRARFileInfo info, fileListR)
		{
			if (info.unpSize > 0)
			{
				bool valid = true;
				for (int e = 0; e < ignored_extensions.length(); e++)
				{
					if (info.fileName.endsWith(ignored_extensions[e], Qt::CaseInsensitive))
					{
						valid = false;
						break;
					}
				}
				if (valid)
					spine.append(info.fileName);
			}
		}
	}

	return true;
}

QString ComicReader::hash()
{
	if (_hash != "")
		return _hash;

	if (type == TYPE_CBZ && (!zip || !zip->isOpen()))
		return _hash;

	if (type == TYPE_CBR && (!rar || !rar->isOpen()))
		return _hash;

	QByteArray CRCarray;
	QDataStream CRCstream(&CRCarray, QIODevice::WriteOnly);

	if (type == TYPE_CBZ)
	{
		QList<QuaZipFileInfo> fileList = zip->getFileInfoList();
		foreach (const QuaZipFileInfo info, fileList)
		{
			CRCstream << info.crc;
		}
	}
	if (type == TYPE_CBR)
	{
		QList<QtRARFileInfo> fileList = rar->fileInfoList();
		foreach (const QtRARFileInfo info, fileList)
		{
			CRCstream << info.fileCRC;
		}
	}

	_hash = QCryptographicHash::hash(CRCarray, QCryptographicHash::Md5).toHex();
	return _hash;
}

QString ComicReader::title()
{
	return _title;
}

void ComicReader::serveComponent(int pagenum, QHttpResponse *response)
{
	if ((type == TYPE_CBZ && (!zip || !zip->isOpen())) || (type == TYPE_CBR && (!rar || !rar->isOpen())))
	{
		response->writeHead(500);
		response->end("Comic file not open for reading");
		return;
	}
	// we start from 0 for the first page
	pagenum = pagenum - 1;

	if (pagenum < 0 || pagenum >= numberOfPages())
	{
		qDebug() << "WARN: Requested page out of range: " << pagenum;
		pagenum = 0;
	}

	QByteArray page;

	if (type == TYPE_CBZ)
	{
		zip->setCurrentFile(spine[pagenum]);
		QuaZipFile zfile(zip);
		if (!zfile.open(QIODevice::ReadOnly))
		{
			response->writeHead(404);
			response->end(QString("Could not find \"" + spine[pagenum] + "\" in file").toUtf8());
			return;
		}
		page = zfile.readAll();
		zfile.close();
	}
	else
	{
		QtRARFile rfile(rar);
		rfile.setFileName(spine[pagenum]);
		if (!rfile.open(QIODevice::ReadOnly))
		{
			response->writeHead(404);
			response->end(QString("Could not find \"" + spine[pagenum] + "\" in file").toUtf8());
			return;
		}
		page = rfile.readAll();
		rfile.close();
	}

	response->setHeader("Content-Type", guessMimeType(spine[pagenum]));
	response->writeHead(200);
	// Important -- use write instead of end, so binary data doesn't get messed up!
	response->write(page);
	response->end();
}

QVariantList ComicReader::getContents()
{
	QVariantList res;
	//     for (int i=0; i<spine.length(); i++) {
	//         QVariantMap entry;
	//         entry["title"] = "%PAGE% " + QString::number(i + 1);
	//         entry["src"] = spine[i];
	//         res.append(entry);
	//     }
	emit contentsReady(res);
	return res;
}

QVariantMap ComicReader::getCoverInfo(int thumbsize, int fullsize)
{
	QVariantMap res;
	if (type == TYPE_CBZ && (!zip || !zip->isOpen()))
		return res;
	if (type == TYPE_CBR && (!rar || !rar->isOpen()))
		return res;

	res["title"] = _title;
	res["author"] = "";
	res["authorsort"] = "zzznone";
	res["cover"] = "ZZZnone";

	res["language"] = "";
	res["description"] = "";
	res["subject"] = "";

	QImage coverimg;
	if (type == TYPE_CBZ)
	{
		zip->setCurrentFile(spine[0]);
		QuaZipFile zfile(zip);
		if (!zfile.open(QIODevice::ReadOnly))
			return res;
		if (!coverimg.loadFromData(zfile.readAll()))
		{
			zfile.close();
			return res;
		}
		zfile.close();
	}
	else
	{
		QtRARFile rfile(rar);
		rfile.setFileName(spine[0]);
		if (!rfile.open(QIODevice::ReadOnly))
			return res;
		if (!coverimg.loadFromData(rfile.readAll()))
		{
			rfile.close();
			return res;
		}
		rfile.close();
	}

	QByteArray byteArray;
	QBuffer buffer(&byteArray);
	coverimg.scaledToWidth(thumbsize, Qt::SmoothTransformation).save(&buffer, "PNG");
	res["cover"] = "data:image/png;base64," + QString(byteArray.toBase64());
	QByteArray byteArrayf;
	QBuffer bufferf(&byteArrayf);
	coverimg.scaledToWidth(fullsize, Qt::SmoothTransformation).save(&bufferf, "PNG");
	res["fullcover"] = "data:image/png;base64," + QString(byteArrayf.toBase64());
	return res;
}
