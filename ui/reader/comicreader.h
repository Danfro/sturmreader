 /* Copyright 2021 Emanuele Sorce emanuele.sorce@hotmail.com
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */


#ifndef COMICREADER_H
#define COMICREADER_H

#include <QObject>
#include <QDomDocument>
#include <QVariant>
#include <QString>
#include <quazip.h>
#include "../qhttpserver/qhttpresponse.h"
#include "../qtrar/src/qtrar.h"

 class ComicReader : public QObject
 {
     Q_OBJECT
     Q_PROPERTY(QString hash READ hash)
     Q_PROPERTY(QString title READ title)
 public:
     explicit ComicReader(QObject *parent = 0);
     QString hash();
     QString title();
     Q_INVOKABLE bool load(const QString &filename);
     Q_INVOKABLE int numberOfPages();
     Q_INVOKABLE void serveComponent(int pagenum, QHttpResponse *response);
     Q_INVOKABLE QVariantMap getCoverInfo(int thumbsize, int fullsize);
     Q_INVOKABLE QVariantList getContents();

 signals:
     void contentsReady(QVariantList contents);

 private:
    bool parse();

    QString _title;
    int type;

     QuaZip *zip;
     QtRAR *rar;

     QString _hash;
     QStringList spine;
};

#endif
